package de.kks.kugelmatikdebugapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.kks.androidapi.APIHandler;
import de.kks.androidapi.network.Coordinate;
import de.kks.androidapi.network.PosData;

public class ControlActivity extends AppCompatActivity {

    private EditText inputClusterX;
    private EditText inputClusterY;
    private EditText inputSphereX;
    private EditText inputSphereY;

    private SeekBar heightValueSlider;

    private TextView debugText;
    private TextView heightText;

    private Button home;
    private Button fix;

    private APIHandler handler;
    private final int MAX_HEIGHT = 8000;

    private int buffer = 0;
    private int bufferProgress = 0;

    @Override
    protected void onStop() {
        super.onStop();
        if (handler.getBtClient().isConnected()) {
            handler.close();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        if (MainActivity.handler == null) {
            stopActivity("API Handler Error.");
        }
        handler = MainActivity.handler;
        inputClusterX = findViewById(R.id.clusterX);
        inputClusterY = findViewById(R.id.clusterY);
        inputSphereX = findViewById(R.id.sphereX);
        inputSphereY = findViewById(R.id.sphereY);
        home = findViewById(R.id.b_home);
        fix = findViewById(R.id.b_fix);
        heightValueSlider = findViewById(R.id.heightValueSlider);
        debugText = findViewById(R.id.info);
        heightText = findViewById(R.id.heightText);

        home.setOnClickListener(v -> onHomeClick());
        fix.setOnClickListener(v -> onFixClick());
        heightValueSlider.setMax(MAX_HEIGHT);
        heightValueSlider.setProgress(0);

        // Events
        heightValueSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                heightText.setText(String.valueOf(progress));
                if (buffer >= 20) {
                    set(progress);
                    buffer = 0;
                }
                buffer += 1;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                set(seekBar.getProgress());
                buffer = 0;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.getBtClient().close();
    }

    private void onHomeClick() {
        Coordinate coordinate = getCoordsFromInput();
        List<PosData> posData = new ArrayList<>();
        posData.add(new PosData(coordinate));
        if (handler.getBtClient().isConnected()) {
            handler.sendHome(posData);
            debug("Finished!");
        } else {
            stopActivity("Connection Error.");
        }
    }

    private void onFixClick() {
        Coordinate coordinate = getCoordsFromInput();
        List<PosData> posData = new ArrayList<>();
        posData.add(new PosData(coordinate));
        if (handler.getBtClient().isConnected()) {
            handler.fix(posData);
            debug("Finished!");
        } else {
            stopActivity("Connection Error.");
        }
    }

    private void set(int height) {
        Coordinate coordinate = getCoordsFromInput();
        List<PosData> posData = new ArrayList<>();
        posData.add(new PosData(coordinate, (short) (height)));
        if (handler.getBtClient().isConnected()) {
            handler.setSpheres(posData);
            debug("Finished!");
        } else {
            stopActivity("Connection Error.");
        }
    }

    private Coordinate getCoordsFromInput() {
        int clusterX;
        int clusterY;
        int sphereX;
        int sphereY;
        try {
            clusterX = Integer.parseInt(inputClusterX.getText().toString());
        } catch (NumberFormatException e) {
            clusterX = 0;
        }
        try {
            clusterY = Integer.parseInt(inputClusterY.getText().toString());
        } catch (NumberFormatException e) {
            clusterY = 0;
        }
        try {
            sphereX = Integer.parseInt(inputSphereX.getText().toString());
        } catch (NumberFormatException e) {
            sphereX = 0;
        }
        try {
            sphereY = Integer.parseInt(inputSphereY.getText().toString());
        } catch (NumberFormatException e) {
            sphereY = 0;
        }
        return new Coordinate(clusterX - 1, clusterY - 1, sphereX - 1, sphereY - 1);
    }

    private void stopActivity(String errorMsg) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("error_msg", errorMsg);
        intent.putExtra("dontCreateHandler", "");
        startActivity(intent);
    }

    private void debug(String msg) {
        if (debugText.getVisibility() != View.VISIBLE) {
            debugText.setVisibility(View.VISIBLE);
        }
        debugText.setText(msg);
    }

    private void resetDebug() {
        if (debugText.getVisibility() == View.VISIBLE) {
            debugText.setVisibility(View.INVISIBLE);
        }
    }
}