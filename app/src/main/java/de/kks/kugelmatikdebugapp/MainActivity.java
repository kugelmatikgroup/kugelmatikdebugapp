package de.kks.kugelmatikdebugapp;

import androidx.annotation.RequiresPermission;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import de.kks.androidapi.APIHandler;
import de.kks.androidapi.network.events.OnBondListener;
import de.kks.androidapi.network.events.OnConnectListener;
import de.kks.androidapi.network.exceptions.BluetoothDisableException;
import de.kks.androidapi.network.exceptions.BluetoothNotSupportedException;

public class MainActivity extends Activity {

    // TODO use Intent instead
    public static APIHandler handler;

    private TextView debugText;
    private Spinner modeSelector;

    @Override
    protected void onStart() {
        super.onStart();
        debugText.setText(getResources().getString(R.string.debug_text));
    }

    @Override
    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        debugText = findViewById(R.id.debugText);
        if (getIntent().getStringExtra("error_msg") != null) {
            debugText.setText(getIntent().getStringExtra("error_msg"));
        }

        if (getIntent().getStringExtra("dontCreateHandler") == null) {
            handler = new APIHandler(this);
        }

        Button connect = findViewById(R.id.connect);
        connect.setOnClickListener(this::onConnectClick);

        Button settings = findViewById(R.id.settings);
        settings.setOnClickListener(v -> startSettingsActivity());

        modeSelector = findViewById(R.id.modeSelector);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.mode_items, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item);
        modeSelector.setAdapter(adapter);
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    private void onConnectClick(View view) {
        try {
            handler.connect(new OnBondListener() {
                @Override
                public void onBond() {
                    debugText.setText("Bonded!");
                }

                @Override
                public void onBondStart() {
                    debugText.setText("Start pairing...");
                }

                @Override
                public void onBondFailed() {
                    debugText.setText("Bonded failed!");
                }
            }, new OnConnectListener() {
                @Override
                public void onConnect() {
                    debugText.setText("Connected!");
                    if (modeSelector.getSelectedItem().toString().equalsIgnoreCase("Debug")) {
                        startControlActivity();
                    } else {
                        startGyroskopActivity();
                    }
                }

                @Override
                public void onConnectError() {
                    debugText.setText("Connect Error");
                }
            });
        } catch (BluetoothDisableException e) {
            debugText.setText("Bluetooth disabled!");
            e.printStackTrace();
        } catch (BluetoothNotSupportedException e) {
            debugText.setText("Bluetooth is not supported!");
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            debugText.setText("Invalid bluetooth adress!");
            e.printStackTrace();
        }
    }

    private void startSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("host", handler.getBtClient().getHost());
        intent.putExtra("serviceID", handler.getBtClient().getServiceID().toString());
        startActivity(intent);
    }

    private void startGyroskopActivity() {
        // TODO
        Intent intent = new Intent(this, null);
        startActivity(intent);
    }

    private void startControlActivity() {
        Intent intent = new Intent(this, ControlActivity.class);
        startActivity(intent);
    }
}