package de.kks.kugelmatikdebugapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {
    
    EditText hostEdit;
    EditText serviceEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        hostEdit = findViewById(R.id.hostEdit);
        hostEdit.setText(MainActivity.handler.getBtClient().getHost());
        serviceEdit = findViewById(R.id.serviceEdit);
        serviceEdit.setText(MainActivity.handler.getBtClient().getServiceID().toString());

        Button back = findViewById(R.id.back);
        back.setOnClickListener(v -> stopActivity());
        Button apply = findViewById(R.id.apply);
        apply.setOnClickListener(v -> onApplyClick());
    }


    private void onApplyClick() {
        try {
            if (hostEdit.getText().toString().replace(" ", "").length() == 0) {
                Toast.makeText(this, R.string.toast_invalid_host, Toast.LENGTH_SHORT).show();
                return;
            }
            if (serviceEdit.getText().toString().replace(" ", "").length() == 0) {
                Toast.makeText(this, R.string.toast_invalid_service, Toast.LENGTH_SHORT).show();
                return;
            }

            MainActivity.handler.getBtClient().setHost(hostEdit.getText().toString());
            MainActivity.handler.getBtClient().setServiceID(serviceEdit.getText().toString());
            Toast.makeText(this, R.string.toast_successful_apply, Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, R.string.toast_invalid_service, Toast.LENGTH_SHORT).show();
        }
    }

    private void stopActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("dontCreateHandler", "");
        startActivity(intent);
    }
}